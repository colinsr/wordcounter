﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using WordCounterTests;

namespace WordCounterConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string text = File.ReadAllText(baseDirectory + "\\input.txt");

            List<WordAndCount> wordAndCounts = WordCounter.Count(text);

            var wordCountStringArray = ConvertWordAndCountToStringArray(wordAndCounts);

            File.WriteAllLines(baseDirectory + "\\output.txt", wordCountStringArray);
        }

        private static string[] ConvertWordAndCountToStringArray(List<WordAndCount> wordAndCounts)
        {
            int numberOfWords = wordAndCounts.Count;
            var wordCountStringArray = new string[numberOfWords];

            for (int i = 0; i < numberOfWords; i++)
            {
                WordAndCount wordAndCount = wordAndCounts[i];
                string resultForWord = wordAndCount.Word + " Found\t" + wordAndCount.Frequency + " times.";
                wordCountStringArray[i] = resultForWord;
            }

            return wordCountStringArray;
        }
    }
}
