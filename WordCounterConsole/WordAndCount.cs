﻿namespace WordCounterTests
{
    public class WordAndCount
    {
        public string Word { get; set; }
        public int Frequency { get; set; }
    }
}