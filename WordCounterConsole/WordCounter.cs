using System;
using System.Collections.Generic;
using System.Linq;

namespace WordCounterTests
{
    public static class WordCounter
    {
        public static List<WordAndCount> Count(string textToCount)
        {
            string cleanedTextToCount = CleanText(textToCount);

            string[] wordsAsArray = SplitOnSpacesCommasOrPeriods(cleanedTextToCount);

            Dictionary<string, int> wordCount = new Dictionary<string, int>();

            foreach (var word in wordsAsArray)
            {
                AddWordOrIncrementCount(wordCount, word);
            }

            return MapDictionaryToOrderedWordAndCountList(wordCount);
        }

        private static void AddWordOrIncrementCount(Dictionary<string, int> wordCount, string word)
        {
            if (!wordCount.ContainsKey(word))
            {
                wordCount.Add(word, 1);
            }
            else
            {
                IncrementCountForWord(wordCount, word);
            }
        }

        private static List<WordAndCount> MapDictionaryToOrderedWordAndCountList(Dictionary<string, int> wordCount)
        {
            return wordCount.Select(wordAndCount => new WordAndCount
                    {
                        Word = wordAndCount.Key, Frequency = wordAndCount.Value
                    }).OrderByDescending(w => w.Frequency).ToList();
        }

        private static string[] SplitOnSpacesCommasOrPeriods(string cleanedTextToCount)
        {
            return cleanedTextToCount.Split(new string[] { " ", ".", "," }, StringSplitOptions.RemoveEmptyEntries);
        }

        private static string CleanText(string textToCount)
        {
            return textToCount.Replace("\"", "").Replace("?", "").Replace("\n", "");
        }

        private static void IncrementCountForWord(Dictionary<string, int> wordCount, string word)
        {
            int curCount = wordCount[word];
            wordCount[word] = curCount + 1;
        }
    }
}