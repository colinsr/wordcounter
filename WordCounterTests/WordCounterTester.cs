﻿using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace WordCounterTests
{
    [TestFixture]
    public class WordCounterTester
    {
        [Test]
        public void Given_One_Word_Return_Word_And_Count()
        {
            string word = "WASHINGTON";
            var expected = GetExpectedOneWordAndCount();

            var actual = WordCounter.Count(word);

            actual.ShouldBeEquivalentTo(expected);
        }

        [Test]
        public void Given_Two_Words_Return_Words_And_Count_Of_Each()
        {
            string word = "WASHINGTON Unable";
            var expected = GetExpectedTwoWordsAndCounts();

            var actual = WordCounter.Count(word);

            actual.ShouldBeEquivalentTo(expected);
        }

        private static List<WordAndCount> GetExpectedTwoWordsAndCounts()
        {
            return new List<WordAndCount> { new WordAndCount {Word = "WASHINGTON", Frequency = 1 }, new WordAndCount{ Word = "Unable", Frequency = 1} };
        }

        [Test]
        public void Given_Bunch_Of_Words_Return_Words_And_Count_Of_Each()
        {
            string word = "WASHINGTON Unable about, uninterrupted. WASHINGTON";

            var expected = GetExpectedFourWordsAndCounts();

            var actual = WordCounter.Count(word);

            actual.ShouldBeEquivalentTo(expected);
        }

        [Test]
        public void Given_Bunch_Of_Words_With_Assorted_Punctuation_Including_Quotes_Return_Words_And_Count_Of_Each()
        {
            string word = "WASHINGTON Unable about, uninterrupted. WASHINGTON \"Why won't it just tell me what it's about?\" said Boston resident";

            var expected = GetExpectedFourteenWordsWithPunctuationAndCounts();

            var actual = WordCounter.Count(word);

            actual.ShouldBeEquivalentTo(expected);
        }

        [Test]
        public void Given_Two_Words_Return_Words_And_Count_Of_Each_In_Descending_Order()
        {
            string word = "Unable about, uninterrupted. WASHINGTON \"Why WASHINGTON won't it just tell me what it's about?\" said Boston resident";

            var expected = GetOrderedExpectedFifteenWordsWithPunctuation();

            var actual = WordCounter.Count(word);

            actual.ShouldBeEquivalentTo(expected);
        }

        private static List<WordAndCount> GetOrderedExpectedFifteenWordsWithPunctuation()
        {
            return new List<WordAndCount>
            {
                new WordAndCount {Word =  "WASHINGTON", Frequency = 2 }, 
                new WordAndCount {Word =  "about", Frequency = 2 }, 
                new WordAndCount {Word =  "Unable", Frequency = 1 }, 
                new WordAndCount {Word =  "uninterrupted", Frequency = 1 },
                new WordAndCount {Word = "Why", Frequency = 1},
                new WordAndCount {Word = "won't", Frequency = 1},
                new WordAndCount {Word = "it", Frequency = 1},
                new WordAndCount {Word = "just", Frequency = 1},
                new WordAndCount {Word = "tell", Frequency = 1},
                new WordAndCount {Word = "me", Frequency = 1},
                new WordAndCount {Word = "what", Frequency = 1},
                new WordAndCount {Word = "it's", Frequency = 1},
                new WordAndCount {Word = "said", Frequency = 1},
                new WordAndCount {Word = "Boston", Frequency = 1},
                new WordAndCount {Word = "resident", Frequency = 1}, 
            };
        }

        private static List<WordAndCount> GetExpectedFourWordsAndCounts()
        {
            return new List<WordAndCount> 
            {   
                new WordAndCount{ Word = "WASHINGTON", Frequency = 2 },
                new WordAndCount{ Word = "Unable", Frequency = 1 }, 
                new WordAndCount{ Word = "about", Frequency = 1 }, 
                new WordAndCount(){ Word = "uninterrupted", Frequency = 1 } 
            };
        }

        private static List<WordAndCount> GetExpectedFourteenWordsWithPunctuationAndCounts()
        {
            return new List<WordAndCount>
            {
                new WordAndCount { Word = "WASHINGTON", Frequency = 2 }, 
                new WordAndCount { Word = "Unable", Frequency = 1 }, 
                new WordAndCount { Word = "about", Frequency = 2 }, 
                new WordAndCount { Word = "uninterrupted", Frequency = 1 },
                new WordAndCount { Word = "Why", Frequency = 1},
                new WordAndCount { Word = "won't", Frequency = 1},
                new WordAndCount { Word = "it", Frequency = 1},
                new WordAndCount { Word = "just", Frequency = 1},
                new WordAndCount { Word = "tell", Frequency = 1},
                new WordAndCount { Word = "me", Frequency = 1}, 
                new WordAndCount { Word = "what", Frequency = 1},
                new WordAndCount { Word = "it's", Frequency = 1},
                new WordAndCount { Word = "said", Frequency = 1},
                new WordAndCount { Word = "Boston", Frequency = 1}, 
                new WordAndCount { Word = "resident", Frequency = 1}, 
            };
        }

        private static List<WordAndCount> GetExpectedOneWordAndCount()
        {
            return new List<WordAndCount> { new WordAndCount { Word = "WASHINGTON", Frequency = 1 } };
        }
    }
}
